﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gems : MonoBehaviour
{
    //Set when added to gameObject, tells what gem this is
    public bool isBlue;
    public bool isYellow;
    public bool isGreen;
    public bool isRed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (isBlue)
        {
            if (GameManager.instance.hasBlue)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.gameObject.SetActive(true);
            }
        }
        else if (isYellow)
        {
            if (GameManager.instance.hasYellow)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.gameObject.SetActive(true);
            }
        }
        else if (isGreen)
        {
            if (GameManager.instance.hasGreen)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.gameObject.SetActive(true);
            }
        }
        else if (isRed)
        {
            if (GameManager.instance.hasRed)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.gameObject.SetActive(true);
            }
        }

    }

    public void Collect()
    {
        AudioController.instance.Collect(transform.position);
        if(isBlue)
        {
            GameManager.instance.hasBlue = true;
            GameManager.instance.UpdateGems();
        }
        else if(isYellow)
        {
            GameManager.instance.hasYellow = true;
            GameManager.instance.UpdateGems();
        }
        else if(isGreen)
        {
            GameManager.instance.hasGreen = true;
            GameManager.instance.UpdateGems();
        }
        else if(isRed)
        {
            GameManager.instance.hasRed = true;
            GameManager.instance.UpdateGems();
        }

    }

}
