﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //Lose the game on trigger enter
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") //Check to see if the player entered the trigger
        {
            GameManager.instance.SetState((int)GameManager.GameState.Lose);
        }
    }

}
