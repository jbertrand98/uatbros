﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimePawn : Pawn
{
    public int life;
    //How long between shots
    public float shotDelay;
    float nextShot;

    public GameObject shot;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        if (tf.position.x < GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = false;
            isMovingRight = true;
            sp.flipX = true;
        }
        else if (tf.position.x > GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = true;
            isMovingRight = false;
            sp.flipX = false;
        }
        nextShot = shotDelay + Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Idle()
    {
        if (tf.position.x < GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = false;
            isMovingRight = true;
            sp.flipX = true;
        }
        else if (tf.position.x > GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = true;
            isMovingRight = false;
            sp.flipX = false;
        }
    }

    public override void Shoot()
    {
        nextShot = shotDelay + Time.time; //update next shot
        GameObject bullet = Instantiate<GameObject>(shot); //instantiate a bullet

        if (sp.flipX)
        {
            bullet.transform.position = tf.position + new Vector3(1, 0, 0);
        }
        else if (!sp.flipX)
        {
            bullet.transform.position = tf.position + new Vector3(-1, 0, 0);
        }

    }

    public override void WasHit(GameObject otherObject)
    {
        life--; //lose life
        if (life <= 0)
        {
            AudioController.instance.Hit(tf.position);
            Destroy(this.gameObject);
        }
    }

    public override int CheckState()
    {
        if (nextShot >= Time.time) //if it has to wait to shoot
        {
            return 0;
        }
        else if (nextShot < Time.time) //if it does not need to wait to shoot
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public override void PlayAnimation(int animationState)
    {
        if (animationState == 0)
        {
            anim.Play("slime_idleCycle");
        }
        else if (animationState == 1)
        {
            anim.Play("slime_shootCycle");
        }
    }

}
