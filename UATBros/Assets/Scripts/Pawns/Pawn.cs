﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour //Base pawn class
{
    //GameObjects transform 
    public Transform tf;
    //GameObjects rigidbody2d
    public Rigidbody2D rb2d;
    //GameObjects animator
    public Animator anim;
    //GameObjects sprite
    public SpriteRenderer sp;
    //Variables for movement logic
    public float moveSpeed;
    public float jumpForce;
    //Variables to show if the player is moving
    public bool isMovingRight = false;
    public bool isMovingLeft = false;


    // Start is called before the first frame update
    public virtual void Start()
    {
        tf = GetComponent<Transform>();
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sp = GetComponent<SpriteRenderer>();
    }

    public virtual void MoveRight()
    {

    }

    public virtual void MoveLeft()
    {

    }

    public virtual void MoveTowards(Vector3 target)
    {

    }

    public virtual void Idle()
    {

    }

    public virtual void Jump()
    {

    }

    public virtual void Shoot()
    {

    }

    public virtual bool IsGrounded()
    {
        return false;
    }

    public virtual void WasHit(GameObject otherObject)
    {
        
    }

    public virtual int CheckState()
    {
        return 0;
    }

    public virtual void PlayAnimation(int animationState)
    {

    }
}
