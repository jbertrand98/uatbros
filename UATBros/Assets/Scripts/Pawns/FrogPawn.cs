﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogPawn : Pawn
{
    //How long the frog will stay on the ground before jumping again
    public float waitTime;
    float groundTime;

    public int life;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        if (tf.position.x < GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = false;
            isMovingRight = true;
            sp.flipX = true;
        }
        else if (tf.position.x > GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = true;
            isMovingRight = false;
            sp.flipX = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void MoveLeft()
    {
        tf.Translate(Vector2.left * moveSpeed * Time.deltaTime);
        groundTime = waitTime + Time.time; //update ground time
    }

    public override void MoveRight()
    {
        tf.Translate(Vector2.right * moveSpeed * Time.deltaTime);
        groundTime = waitTime + Time.time; //update ground time
    }

    public override void Idle()
    {
        if (transform.position.x < GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = false;
            isMovingRight = true;
            sp.flipX = true;
        }
        else if (transform.position.x > GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = true;
            isMovingRight = false;
            sp.flipX = false;
        }
    }

    public override void Jump()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce); //add force upwards
    }

    public override bool IsGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector2.down, 0.38f); //Raycast downwards

        if (hitInfo.collider != null) //if the raycast hit anything
        {
            return true;
        }
        return false;
    }

    public override void WasHit(GameObject otherObject)
    {
        life--; //lose life
        if (life <= 0)
        {
            AudioController.instance.Hit(tf.position);
            Destroy(this.gameObject);
        }
    }

    public override int CheckState()
    {
        if (IsGrounded() && groundTime >= Time.time) //if the frog has not been grounded long enough
        {
            return (int)FrogController.FrogState.Idle;
        }
        else if (IsGrounded() && groundTime < Time.time) //if the frog has been grounded long enough
        {
            return (int)FrogController.FrogState.Jump;
        }
        else if (!IsGrounded()) //if the frog is not grounded
        {
            return (int)FrogController.FrogState.AirBorne;
        }
        else
        {
            return (int)FrogController.FrogState.Idle;
        }
    }

    public override void PlayAnimation(int animationState)
    {
        if ((FrogController.FrogState)animationState == FrogController.FrogState.Idle)
        {
            anim.Play("frog_standCycle");
        }
        else if ((FrogController.FrogState)animationState == FrogController.FrogState.Jump)
        {
            anim.Play("frog_jumpCycle");
        }
        else if ((FrogController.FrogState)animationState == FrogController.FrogState.AirBorne)
        {
            anim.Play("frog_jumpCycle");
        }

    }

}
