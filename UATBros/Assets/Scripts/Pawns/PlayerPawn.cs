﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    public float hitStun;
    public float stunTime;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void MoveRight()
    {
        tf.Translate(moveSpeed * Time.deltaTime * Vector2.right);
        isMovingRight = true;
        isMovingLeft = false;
    }

    public override void MoveLeft()
    {
        tf.Translate(moveSpeed * Time.deltaTime * Vector2.left);
        isMovingRight = false;
        isMovingLeft = true;
    }

    public override void Idle()
    {
        isMovingRight = false;
        isMovingLeft = false;
    }

    public override void Jump()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce); //Add force upwards
        AudioController.instance.Jump(tf.position); //Play sound effect
    }

    public override void Shoot()
    {
        GameObject fireball = Instantiate<GameObject>(GameManager.instance.fireball); //Instantiate a fireball
        GameManager.instance.activeFireballs.Add(fireball); //Add to list of active fireballs

        if (sp.flipX)
        {
            fireball.transform.position = tf.position + new Vector3(-1, 0, 0);
        }
        else if (!sp.flipX)
        {
            fireball.transform.position = tf.position + new Vector3(1, 0, 0);
        }

        AudioController.instance.Shoot(tf.position); //Play sound effect
    }

    public override bool IsGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector2.down, 0.75f); //Raycast downwards

        if (hitInfo.collider != null) //If the raycast hit something
        {
            return true;
        }
        return false;
    }

    public override void WasHit(GameObject otherObject)
    {
        if (stunTime < Time.time) //Check to make sure we are not already in hitstun
        {
            AudioController.instance.Hit(tf.position); //Play sound effect
            GameManager.instance.lives--; 
            GameManager.instance.UpdateLives();
            stunTime = hitStun + Time.time; //Put in hitstun
            KnockBack();
        }

        Destroy(otherObject);
    }

    private void KnockBack()
    {
        if (sp.flipX)
        {
            rb2d.velocity = new Vector2(4, 0); //Add force backwards
        }
        else if (!sp.flipX)
        {
            rb2d.velocity = new Vector2(-4, 0); //Add force backwards
        }
    }

    public override int CheckState()
    {
        
        if (stunTime >= Time.time) //if in hitstun
        {
            return (int)PlayerController.PlayerState.Hit;
        }
        else if (!IsGrounded()) //if not grounded
        {
            return (int)PlayerController.PlayerState.Jump;
        }
        else if (isMovingRight) //if moving right
        {
            sp.flipX = false;
            return (int)PlayerController.PlayerState.Walk;
        }
        else if (isMovingLeft) //if moving left
        {
            sp.flipX = true;
            return (int)PlayerController.PlayerState.Walk;
        }
        else
        {
            return (int)PlayerController.PlayerState.Stand;
        }

    }

    public override void PlayAnimation(int animState)
    {
        if((PlayerController.PlayerState)animState == PlayerController.PlayerState.Hit)
        {
            anim.Play("alienPink_hitCycle");
        }
        else if ((PlayerController.PlayerState)animState == PlayerController.PlayerState.Jump)
        {
            anim.Play("alienPink_jumpCycle");
        }
        else if ((PlayerController.PlayerState)animState == PlayerController.PlayerState.Walk)
        {
            anim.Play("alienPink_walkCycle");
        }
        else if ((PlayerController.PlayerState)animState == PlayerController.PlayerState.Stand)
        {
            anim.Play("alienPink_standCycle");
        }
    }


}
