﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearPawn : Pawn
{
    public int life;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        if (tf.position.x < GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = false;
            isMovingRight = true;
        }
        else if (tf.position.x > GameManager.instance.playerPawn.tf.position.x)
        {
            isMovingLeft = true;
            isMovingRight = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void MoveLeft()
    {
        tf.Translate(Vector2.left * moveSpeed * Time.deltaTime);
        if(HitWall()) //if it hits a wall
        {
            //change direction
            isMovingLeft = !isMovingLeft;
            isMovingRight = !isMovingRight;
        }
    }

    public override void MoveRight()
    {
        tf.Translate(Vector2.right * moveSpeed * Time.deltaTime);
        if (HitWall()) //if it hits a wall
        {
            //change direction
            isMovingLeft = !isMovingLeft;
            isMovingRight = !isMovingRight;
        }
    }

    public override void WasHit(GameObject otherObject)
    {
        life--; //Take damage
        if (life <= 0)
        {
            AudioController.instance.Hit(tf.position);
            Destroy(this.gameObject);
        }
    }

    private bool HitWall()
    {
        RaycastHit2D hitInfo;
        if (isMovingLeft)
        {
            hitInfo = Physics2D.Raycast(tf.position, Vector2.left, 0.59f); //Raycast to the left
        }
        else
        {
            hitInfo = Physics2D.Raycast(tf.position, Vector2.right, 0.59f); //Raycast to the right
        }


        if (hitInfo.collider != null) //If the raycast hit something
        {
            return true;
        }
        return false;
    }

}
