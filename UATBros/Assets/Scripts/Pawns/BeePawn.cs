﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeePawn : Pawn
{
    Vector3 direction;
    public int life;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void MoveTowards(Vector3 target)
    {
        if (tf.position.x < target.x)
        {
            isMovingLeft = false;
            isMovingRight = true;
            sp.flipX = true;
        }
        else if (tf.position.x > target.x)
        {
            isMovingLeft = true;
            isMovingRight = false;
            sp.flipX = false;
        }

        //get direction to player
        direction = target - tf.position; 
        direction.Normalize();

        tf.Translate(direction * moveSpeed * Time.deltaTime); //move towards player
    }

    public override void WasHit(GameObject otherObject)
    {
        life--; //lose life
        if (life <= 0)
        {
            AudioController.instance.Hit(tf.position);
            Destroy(this.gameObject);
        }
    }

}
