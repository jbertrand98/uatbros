﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.instance.playerPawn.sp.flipX)
        {
            direction = Vector2.left;
        }
        else if (!GameManager.instance.playerPawn.sp.flipX)
        {
            direction = Vector2.right;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * GameManager.instance.fireballSpeed * Time.deltaTime);
    }

    public void OffScreen()
    {
        GameManager.instance.activeFireballs.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject otherObject = collision.gameObject;
        Pawn otherPawn = otherObject.GetComponent<Pawn>();

        if (otherPawn != null)
        {
            otherPawn.WasHit(this.gameObject);
        }

        GameManager.instance.activeFireballs.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

}
