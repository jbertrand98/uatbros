﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    //The prefab to be spawned
    public GameObject enemyToSpawn;
    Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn()
    {
        GameObject newEnemy = Instantiate(enemyToSpawn, tf); //instantiate new enemy
        GameManager.instance.activeEnemies.Add(newEnemy); //add to active enemies list
    }

}
