﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Transform tf;
    public float bulletSpeed;
    Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        //target the player
        target = GameManager.instance.playerPawn.tf.position - tf.position;
        target.Normalize();
    }

    // Update is called once per frame
    void Update()
    {
        tf.Translate(target * bulletSpeed * Time.deltaTime);
    }

    public void OffScreen()
    {
        Destroy(this.gameObject);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject otherObject = collision.gameObject;
        Pawn otherPawn = otherObject.GetComponent<Pawn>();

        if (otherPawn != null)
        {
            otherPawn.WasHit(this.gameObject);
        }

        Destroy(this.gameObject);
    }

}
