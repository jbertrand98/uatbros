﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    //Singleton
    public static AudioController instance;

    //Audio clips
    public AudioClip jump;
    public AudioClip shoot;
    public AudioClip hit;
    public AudioClip collect;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Jump(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(jump, position);
    }

    public void Shoot(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(shoot, position);
    }

    public void Hit(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(hit, position);
    }

    public void Collect(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(collect, position, 10);
    }

}
