﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    //Gamestate enum
    public enum GameState
    {
        Menu, //0
        Game, //1
        About, //2
        Win, //3
        Lose, //4
        Quit //5
    }
    public GameState gameState;

    public List<GameObject> stateObjects;

    //Player object and its components
    public GameObject player;
    public PlayerController playerController;
    public PlayerPawn playerPawn;
    Transform playerTF;
    Vector3 playerHome;

    //player lives
    public int lives;

    //Fireball data
    public GameObject fireball;
    public float fireballSpeed;
    public List<GameObject> activeFireballs;
    public int maxFireballs;

    //Active enemies
    public List<GameObject> activeEnemies;

    //health images
    public List<GameObject> lifeDisplay;

    //HUD gem images
    public List<GameObject> gemDisplay;

    //Gem Objects
    public List<GameObject> gems;

    public bool hasBlue;
    public bool hasYellow;
    public bool hasGreen;
    public bool hasRed;

    //Door objects
    public GameObject closedDoor;
    public GameObject openDoor;

    private void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        SetState(0);

        playerController = player.GetComponent<PlayerController>();
        playerPawn = player.GetComponent<PlayerPawn>();
        playerTF = player.GetComponent<Transform>();
        playerHome = playerTF.position;

        lives = 3;

        hasBlue = false;
        hasYellow = false;
        hasGreen = false;
        hasRed = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Set the game state
    public void SetState(int state)
    {
        gameState = (GameState)state;
        ActivateState();
    }

    //Update the game state
    void ActivateState()
    {
        if (gameState == GameState.Quit)
        {
            Application.Quit();
        }

        for(int i = 0; i < stateObjects.Count; i++)
        {
            if (i == (int)gameState)
            {
                stateObjects[i].SetActive(true);
            }
            else
            {
                stateObjects[i].SetActive(false);
            }
        }

    }

    //update the lives hud display
    public void UpdateLives()
    {
        if (lives == 3)
        {
            lifeDisplay[0].SetActive(true);
            lifeDisplay[1].SetActive(false);
            lifeDisplay[2].SetActive(false);
        }
        else if (lives == 2)
        {
            lifeDisplay[0].SetActive(false);
            lifeDisplay[1].SetActive(true);
            lifeDisplay[2].SetActive(false);
        }
        else if (lives == 1)
        {
            lifeDisplay[0].SetActive(false);
            lifeDisplay[1].SetActive(false);
            lifeDisplay[2].SetActive(true);
        }
        else if (lives <= 0)
        {
            SetState((int)GameState.Lose);
        }
    }

    //update the gems hud display
    public void UpdateGems()
    {

        if (!hasBlue)
        {
            gemDisplay[0].SetActive(true);
            gemDisplay[1].SetActive(false);
        }
        else if (hasBlue)
        {
            gemDisplay[0].SetActive(false);
            gemDisplay[1].SetActive(true);
        }

        if (!hasYellow)
        {
            gemDisplay[2].SetActive(true);
            gemDisplay[3].SetActive(false);
        }
        else if (hasYellow)
        {
            gemDisplay[2].SetActive(false);
            gemDisplay[3].SetActive(true);
        }

        if (!hasGreen)
        {
            gemDisplay[4].SetActive(true);
            gemDisplay[5].SetActive(false);
        }
        else if (hasGreen)
        {
            gemDisplay[4].SetActive(false);
            gemDisplay[5].SetActive(true);
        }

        if (!hasRed)
        {
            gemDisplay[6].SetActive(true);
            gemDisplay[7].SetActive(false);
        }
        else if (hasRed)
        {
            gemDisplay[6].SetActive(false);
            gemDisplay[7].SetActive(true);
        }

        if (hasBlue && hasYellow && hasGreen && hasRed)
        {
            closedDoor.SetActive(false);
            openDoor.SetActive(true);
        }

    }

    //Reset the game
    public void ResetGame()
    {
        playerTF.position = playerHome;
        lives = 3;

        hasBlue = false;
        hasYellow = false;
        hasGreen = false;
        hasRed = false;

        UpdateLives();
        UpdateGems();

        for (int i = 0; i < gems.Count; i++)
        {
            gems[i].SetActive(true);
        }

        closedDoor.SetActive(true);
        openDoor.SetActive(false);

        for (int i = 0; i < activeFireballs.Count; i++)
        {
            Destroy(activeFireballs[i]);
        }

        for (int i = 0; i < activeEnemies.Count; i++)
        {
            Destroy(activeEnemies[i]);
        }
    }
}
