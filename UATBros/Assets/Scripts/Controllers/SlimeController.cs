﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeController : Controller
{
    //Binary state using 0 and 1
    int state;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        state = 0;
    }

    // Update is called once per frame
    void Update()
    {
        state = pawn.CheckState(); //Check what state it should be in
        pawn.PlayAnimation(state); //Playe the right animation

        //Call a pawn function based on what state it is in
        if(state == 0)
        {
            pawn.Idle();
        }
        else if(state == 1)
        {
            pawn.Shoot();
        }

    }
}
