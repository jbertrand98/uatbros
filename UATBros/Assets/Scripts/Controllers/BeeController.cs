﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeController : Controller
{

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        pawn.MoveTowards(GameManager.instance.playerPawn.tf.position);
    }
}
