﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogController : Controller
{
    //Frog state
    public enum FrogState
    {
        Idle, //0
        Jump, //1
        AirBorne //2
    }
    FrogState frogState;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        frogState = FrogState.Idle;  //Start in idle
    }

    // Update is called once per frame
    void Update()
    {
        frogState = (FrogState)pawn.CheckState(); //Check what state it should be in
        pawn.PlayAnimation((int)frogState); //Play the right animation

        //Call a pawn function based on what state the frog is in
        if (frogState == FrogState.Idle)
        {
            pawn.Idle();
        }
        if (frogState == FrogState.Jump)
        {
            pawn.Jump();
        }
        if (frogState == FrogState.AirBorne)
        {
            if (pawn.isMovingLeft)
            {
                pawn.MoveLeft();
            }
            else if (pawn.isMovingRight)
            {
                pawn.MoveRight();
            }
        }

    }
}
