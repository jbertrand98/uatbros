﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{

    //States of the players animation
    public enum PlayerState
    {
        Stand, //0
        Walk, //1
        Jump, //2
        Hit //3
    }
    public PlayerState playerState;


    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        playerState = PlayerState.Stand;
    }

    // Update is called once per frame
    void Update()
    {
        playerState = (PlayerState)pawn.CheckState(); //Check what state should be active
        pawn.PlayAnimation((int)playerState); //Play the animation of the current state

        if (playerState != PlayerState.Hit) //The player cannot do anything while they are in hitstun
        {
            if (Input.GetKey("right") || Input.GetKey("d"))
            {
                pawn.MoveRight(); //Move the player right
            }
            else if (Input.GetKey("left") || Input.GetKey("a"))
            {
                pawn.MoveLeft(); //Move the player left
            }
            else
            {
                pawn.Idle(); //The player is idle
            }

            if (Input.GetKeyDown("left shift") || Input.GetKeyDown("right shift"))
            {
                if (GameManager.instance.activeFireballs.Count < GameManager.instance.maxFireballs) //Make sure the max number of fireballs are not on screen
                {
                    pawn.Shoot();
                }
            }

            if (Input.GetKeyDown("space") && timesJumped < numberOfJumps)
            {
                timesJumped++; //increment the number of jumps used
                pawn.Jump();
            }
        }

        if (!pawn.IsGrounded() && timesJumped == 0) //Check to see if the player has left the ground without jumping
        {
            timesJumped++;
        }

        if (pawn.IsGrounded()) //Check to see if the player is on the ground
        {
            timesJumped = 0;
        }

    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject otherObject = collision.gameObject;
        Pawn otherPawn = otherObject.GetComponent<Pawn>(); //Get the pawn attached to the collided object


        if (otherPawn != null) //Make sure the other object actually has a pawn
        {
            pawn.WasHit(otherObject); 
            otherPawn.WasHit(this.gameObject);
        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherObject = collision.gameObject;
        Gems gems = otherObject.GetComponent<Gems>();  //Get the gems component attached to the collided object

        if (gems != null) //Make sure the other object actually has a gems component
        {
            gems.Collect();
        }

    }

}
