﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearController : Controller
{
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(pawn.isMovingLeft) //Check if it should be moving left
        {
            pawn.MoveLeft();
        }
        else if(pawn.isMovingRight) //Check if it should be moving right
        {
            pawn.MoveRight();
        }
        
    }



}
