﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour //Controller base class
{
    //Pawn to be controlled
    public Pawn pawn;

    //variables for jumping logic
    public int numberOfJumps;
    public int timesJumped;

    // Start is called before the first frame update
    public virtual void Start()
    {
        pawn = GetComponent<Pawn>(); //Get pawn
    }

}
