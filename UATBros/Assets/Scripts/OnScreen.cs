﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        GameObject otherObject = collision.gameObject;
        //Check what left the screen
        Fireball fireball = otherObject.GetComponent<Fireball>();
        Pawn pawn = otherObject.GetComponent<Pawn>();
        Bullet bullet = otherObject.GetComponent<Bullet>();

        if (fireball != null)
        {
            fireball.OffScreen();
        }
        if (bullet != null)
        {
            bullet.OffScreen();
        }
        if (pawn != null)
        {
            Destroy(otherObject);
        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherObject = collision.gameObject;
        //Check if it was a spawner that entered the screen
        SpawnPoint spawnPoint = otherObject.GetComponent<SpawnPoint>();

        if (spawnPoint != null)
        {
            spawnPoint.Spawn();
        }
    }

}
